package mavenExample;

/**
 * Created by carlos on 6/9/17.
 */
public class CodeError {

    private String level,object,message;

    public CodeError(String level, String object, String message) {
        this.level = level;
        this.object = object;
        this.message = message;
    }

    public String getLevel() {
        return this.level;
    }

    public String getObject() {
        return this.object;
    }

    public String getMessage() {
        return this.message;
    }
}
