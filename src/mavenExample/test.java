package mavenExample;
import au.com.codeka.carrot.*;
import au.com.codeka.carrot.resource.*;
import java.util.LinkedList;
import java.util.Map;
import java.util.HashMap;
import java.io.PrintWriter;
import mavenExample.CodeError;



public class test {

	private String reportPath = "report";

	public static LinkedList<CodeError> generateList() {
		LinkedList<CodeError> r = new LinkedList<>();
		for(int i = 0; i < 15; i++) {
			r.add(new CodeError("CN_02","CN_02_Test","Error"));
		};
		return r;
	}

	public static void main(String args[]) {
		CarrotEngine engine = new CarrotEngine();
		Configuration config = engine.getConfig();
		config.setResourceLocater(
				new FileResourceLocater(config, "templates"));

		Map<String, Object> info = new HashMap<>();
		LinkedList<CodeError> tests = test.generateList();
		info.put("tests", tests);

		try {
			PrintWriter out = new PrintWriter("report/index.html");
			out.println(engine.process("index.html", info));
			out.close();

		} catch (Exception e) {
			System.out.println("Fallo");
		}

	}
	
}